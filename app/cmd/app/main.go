package main

import (
	"log"
	"productionService/cmd/internal/app"
	"productionService/cmd/internal/config"
	"productionService/cmd/pkg/logging"
)

func main() {
	log.Print("config init")
	cfg := config.GetConfig()

	log.Print("logger init")
	logger := logging.GetLogger(cfg.AppConfig.LogLevel)

	a, err := app.NewApp(cfg, &logger)
	if err != nil {
		log.Fatal(err)
	}
	logger.Println("Running app")
	a.Run()
}
